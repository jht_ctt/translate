/*
  Localizable.strings
  FitDisplay
  Created by Steve Liao on 2020/7/20.
  Copyright © 2020 Johnsonfitness. All rights reserved.
*/





/* 
 MARK:A
 */ 

"Accessories" = "Accessoires";
"Account" = "Account";
"AccountPlaceholder" = "Voer e-mailadres in";
"Add" = "Toevoegen";
"AddANewDevice" = "Voeg een nieuw apparaat toe";
"AddEquipment" = "Apparatuur toevoegen";
"AddLater" = "Later toevoegen";
"AddMyEquipment" = "Mijn apparatuur toevoegen";
"AddMyEquipmentStep1" = "Sluit uw apparatuur aan op een stroombron en schakel de stroom in.";
"AddMyEquipmentStep2" = "Zorg ervoor dat Bluetooth op uw mobiele apparaat is ingeschakeld.";
"AddOtherEquipment" = "Andere apparatuur toevoegen";
"Advanced" = "GEAVANCEERD";
"Avg.Calories" = "Gem. Calorieën";
"Avg.Speed" = "GEMIDDELDE SNELHEID";
"Avg.HR" = "GEMIDDELDE HARTSLAG";
"AutomaticAdjustments" = "Automatische aanpassingen";
"AuthenticationCode" = "Authenticatiecode";
"AuthenticationCodePlaceholder" = "Voer authenticatiecode in";
"AppTutorial" = "App-tutorial";
"AuthenticationCodeSent" = "We hebben de authenticatiecode zojuist gestuurd naar";
"AscentTrainer" = "Ascent Trainer";
"AddedEquipment" = "Toegevoegde apparatuur";
"Accept" = "Aanvaarden";
"AgeLimitSummary" = "Ik ben 13 jaar oud.";
"Age" = "Leeftijd";
"Accrued" = "Kopen";
"ActiveRecovery" = "Actief Herstel";
"Avg.RPM" = "Avg. RPM";


/* 
 MARK:B
 */ 

"Beginner" = "BEGINNER";
"Birthday" = "Verjaardag";
"Bike" = "Bike";
"Buy" = "Comprar";


/* 
 MARK:C
 */ 

"Calories" = "CALORIEËN";
"CaloriesSummary" = "Hoeveel kunt u verbranden?";
"Cancel" = "Annuleren";
"Continent" = "Continent";
"City" = "Regio";
"Completion" = "Voltooiing";
"Connect" = "Verbinden";
"ConnectApps" = "Verbinding maken met apps";
"Connecting" = "Verbinding maken";
"Connected" = "Verbonden";
"Connection" = "Verbinding";
"ConnectionSummary" = "Maak nu verbinding met de apparatuur.";
"ConnectedHeartSrapDesc" = "Verbonden met %@.";
"Continue" = "Gebruiker Bewerken";
"Cooldown" = "Cool down";
"ConfirmPassword" = "Voer wachtwoord opnieuw in";
"ConfirmPasswordPlaceholder" = "Voer wachtwoord opnieuw in";
"ClimbMill" = "Tredmolen";
"Cardio" = "Cardio";
"ChangePassword" = "Wijzig wachtwoord";
"CheckYourEmailAddress" = "Controleer uw e-mailadres";
"ConnectingFailed" = "Verbinding Maken Mislukt.";
"ConnectingFailSummary1" = "Zorg ervoor dat uw machine is gestopt. Druk drie seconden op de stop-knop op het apparaat om te stoppen";
"ConnectingFailSummary2" = "Bluetooth-verbinding is onderbroken. Zorg ervoor dat het apparaat stand-by staat.";
"CreatePassword" = "Maak een wachtwoord dat:";
"CreateAccount" = "Nieuw account aanmaken";
"Current" = "Huidig";
"CaloriesGoal" = "Caloriedoel";
"ChangeLanguage" = "Wijzig taal";
"CurrentStatus" = "Huidige status";
"ConnectingErrorSummary" = "Er is sprake van een verbindingsprobleem, probeer het opnieuw of neem contact op met de klantenservice.";
"ConnectingError" = "Verbindingsfout";
"Camera" = "Camera";
"CompletedTime" = "%d keer voltooid";
"CompletedTimes" = "%d keer voltooid";
"CurrentSweatScore" = "Huidige Sweat Score";
"ContactUs" = "Neem contact met ons op";
"CrossTrainer" = "Elliptische machine";


/* 
 MARK:D
 */ 

"DeleteAccount" = "Account verwijderen";
"DeleteAccountSummary" = "Weet u zeker dat u uw account wilt verwijderen? Alle gegevens worden gewist";
"Difficulty" = "Moeilijkheid";
"Disconnect" = "Verbinding Verbreken";
"DisconnectSummary" = "Nadat de verbinding met uw muziekaccount is verbroken, kunt u de afspeellijst tijdens uw training niet gebruiken.";
"Distance" = "Afstand";
"DistanceSummary" = "Hoe ver kunt u gaan?";
"Done" = "Gedaan";
"DontHaveAccount" = "Nog geen account?";
"Duration" = "Tijdsduur";
"DistanceGoal" = "Afstandsdoel";


/* 
 MARK:E
 */ 

"EditProfile" = "Profiel bewerken";
"Elite" = "ELITE";
"Equipment" = "Apparatuur";
"EditAccessories" = "Bewerk accessoires";
"EmailAddress" = "E-mailadres";
"EmailAddressHolder" = "Voer e-mailadres in";
"EmailAddressIncorrectSummary" = "“%@” bestaat niet. Bevestig en probeer het opnieuw.";
"EnterCodeForAccount" = "Voer de code in om uw account te activeren.";
"EnterCodeForPassword" = "Voer de code in om uw wachtwoord opnieuw in te stellen.";
"EquipmentType" = "Toesteltype";
"Elliptical" = "Elliptical";
"EnterNewPassword" = "Voer uw nieuwe wachtwoord in";
"EnterOldPassword" = "Voer uw oude wachtwoord in";
"Edit" = "Bewerken";
"ExceedHRTarget" = "Overschrijding HR-doel";
"ExceedHRTargetSummary" = "Uw hartslag overschrijdt het doel te veel. Neem rust voordat u de training hervat.";
"ExceedLimit" = "Overschrijding van de limiet";
"ExceedLimitSummary" = "De trainingsinstelling overschrijdt de limiet van het apparaat.";
"ExceedLimitSummary2" = "Sprint 8 moet worden gebruikt op een loopband met hellingsfunctie";
"ExceedMaxHR" = "Hartslag overschreed max";
"ExceedMaxHRSummary" = "Hartslag overschreed max. Neem rust voordat u de training hervat.";


/* 
 MARK:F
 */ 

"FavoriteWorkout" = "Favoriete training";
"Featured" = "KENMERKEN";
"Female" = "Vrouw";
"AtZoneHelp" = "@Zone Help";
"FirstName" = "Voornaam";
"FirstNamePlaceholder" = "Voornaam invoeren";
"Forget" = "Negeren";
"ForgotPasswordTitle" = "Wachtwoord vergeten";
"ForgotPassword" = "Wachtwoord vergeten?";
"ForgetEquipment" = "Apparatuur vergeten?";
"ForgetEquipmentSummary" = "De apparatuur wordt van de lijst verwijderd. Als u in de toekomst verbinding wilt maken met deze apparatuur, moet u deze opnieuw registreren.";
"ForgotPasswordInstruction" = "We sturen een authenticatiecode naar uw e-mail met de instructie om uw wachtwoord opnieuw in te stellen.";
"FAQ" = "Veelgestelde vragen";
"Filters" = "Filters";
"Filter" = "Filter";
"FreeVideoClass" = "Z Minute";
"ForceUpdateTitle" = "We zijn beter dan ooit.";
"ForceUpdateSummary" = "Een nieuwe versie van de @Zone is
beschikbaar en moet worden voortgezet.";
"Free" = "Gratis";
"FitnessApps" = "Partners";


/* 
 MARK:G
 */ 

"Gender" = "Geslacht";
"GoalWorkout" = "Doeltraining";
"GuidesIntro" = "Hoe kunnen wij u helpen?";
"GoldenGateBridge" = "Golden Gate Bridge";
"Goal" = "Doelen";
"Go" = "Gaan";
"GoPurchase" = "Aankoop";


/* 
 MARK:H
 */ 

"HeartRate" = "Hartslag";
"HeartRateStrap" = "Hartslagband";
"Height" = "Hoogte";
"HIIT" = "HIIT";
"HeartRateLost" = "Hartslag weggevallen";
"HeartRateLostSummary" = "De training stopt en toont een samenvatting in";
"History" = "Geschiedenis";


/* 
 MARK:I
 */ 

"Incline" = "Hellingshoek";
"InclineUnit" = "Hellingshoek\n(%)";
"Intermediate" = "GEMIDDELD";
"Interval" = "Interval";
"IntervalSummary" = "Daag uzelf uit met intervallen.";
"IndoorCycle" = "Indoor fietsen";
"Intervals" = "Intervallen";
"IUnderstand" = "Ik begrijp het";
"InclineDifference" = "Helling wijzigen";
"Imperial" = "Engels";
"IndoorBike" = "Binnenfiets";
"Instructor" = "Instructeur";


/* 
 MARK:J
 */ 

"JoinRace" = "Aan race meedoen";


/* 
 MARK:K
 */ 

"KPH" = "kph";
"KnewIt" = "Begrepen";


/* 
 MARK:L
 */ 

"Language" = "Taal";
"LastName" = "Achternaam";
"LastNamePlaceholder" = "Vul uw achternaam in";
"Level" = "Niveau";
"LogOut" = "Afmelden";
"LoginCredentials" = "Aanmeldingsinformatie";
"LISS" = "LISS (Low Intensity, Steady State; lage intensiteit, stabiele toestand)";
"LastVersionSummary" = "Uw software is up-to-date.";
"LeaderBoardWarning" = "Aangezien u deze training niet hebt voltooid, kunnen uw resultaten niet in de ranglijst worden opgenomen.";
"LoginLockSummary" = "Het account is vergrendeld. Probeer het over 30 seconden opnieuw.";
"LeaderBoard" = "Scorebord";
"Location" = "Locatie";
"Lastest" = "Laatste";


/* 
 MARK:M
 */ 

"Male" = "Man";
"MatchHeartRate" = "Stem uw streefhartslag af op…";
"MeasurementUnit" = "Meeteenheid";
"Monthly" = "Maandelijks";
"Minutes" = "minuten";
"Minute" = "minute";
"MyEquipment" = "Mijn apparatuur";
"MyDevices" = "My Devices";
"Mins" = "minuten";
"MostPopular" = "Meest populair";
"Metric" = "Metriek";
"MPH" = "mph";


/* 
 MARK:N
 */ 

"Name" = "NAAM";
"Nickname" = "Bijnaam";
"NicknamePlaceholder" = "Scorebord-naam";
"NoEquipmentAdded" = "Geen apparatuur toegevoegd";
"NoEquipmentConnectedToatZone" = "Geen apparaat aangesloten";
"NoHeartRateStrapSummary" = "Voor deze training moet u een hartslagband gebruiken.";
"Notification" = "Melding";
"NotConnected" = "Niet verbonden";
"NoEquipment" = "Geen apparaat";
"NewPassword" = "Nieuw wachtwoord";
"NoInternet" = "Geen internetverbinding";
"NoEquipmentSummary" = "Geen apparaat aangesloten";
"NextSpeed" = "Volgende Snelheid";
"NextIncline" = "Volgende Helling";
"NextResistance" = "Volgende Weerstand";
"NotFound" = "Niet gevonden";
"NoBluetoothConnection" = "Geen Bluetooth-verbinding";
"NoBluetoothConnectionSummary" = "Bluetooth-verbinding vereist om door te gaan. Controleer uw verbinding.";
"NotEnough" = "niet genoeg";
"NotProvided" = "Niet verstrekt";
"NoHistory" = "Geen Geschiedenis";
"NotSupportDeviceTitle" = "Dit is alleen voor deze apparaten.";
"NotSupportDeviceSummary" = "U moet de volgende apparaten gebruiken. \nSluit het apparaat aan";


/* 
 MARK:O
 */ 

"OtherDevices" = "Andere apparaten";
"Oops" = "Oeps, er is iets misgegaan";
"OopsSummary" = "Probeer het opnieuw. Als u uw apparatuur nog steeds niet kunt toevoegen, neem dan contact op met de klantenservice voor hulp.";
"OldPassword" = "Oud wachtwoord";
"Ok" = "Ok";
"OnDemand" = "Video op aanvraag";


/* 
 MARK:P
 */ 

"Pace" = "Tempo";
"PaceUnit" = "Tempo (km/uur)";
"PaceSummary" = "Training met uw snelheid.";
"Password" = "Wachtwoord";
"PasswordPlaceholder" = "Wachtwoord invoeren";
"PasswordWillExpire" = "De link voor het opnieuw instellen van het wachtwoord verloopt over 30 minuten.";
"PrivacyPolicies" = "Privacybeleid";
"Profile" = "Profiel";
"PersonalInformation" = "Persoonlijke gegevens";
"PasswordRule1" = "Bevat minimaal acht karakters";
"PasswordRule2" = "Bevat minimaal één cijfer (0–9)";
"PasswordRule3" = "Bevat zowel kleine (a–z) als hoofdletters (A–Z)";
"PrivacyPolicyAgreeSummary" = "Ik ga akkoord met de gebruiksvoorwaarden en het privacybeleid van atZone";
"PrivacyPolicy" = "Privacybeleid";
"Paused" = "Gepauzeerd";
"PausedSummary1" = "De training stopt en toont een samenvatting in";
"PausedSummary2" = "Blijf trappen om door te gaan met de training";
"PresetSpeed" = "Vooraf ingestelde snelheid";
"Purchasing" = "Purchasing...";
"Posted" = "Uitgave";


/* 
 MARK:R
 */ 

"Rank" = "Ranglijst";
"Recovery" = "HERSTEL";
"RequiredEquipment" = "Vereiste apparatuur";
"Resend" = "Opnieuw verzenden";
"ResetPassword" = "Wachtwoord opnieuw instellen";
"ResetAll" = "Alles resetten";
"ReEnterNewPassword" = "Voer nieuw wachtwoord opnieuw in";
"ReEnterPasswordPlaceHolder" = "Voer wachtwoord opnieuw in";
"Region" = "Regio";
"RecentUse" = "Recent gebruik";
"Rename" = "Hernoemen";
"Resistance" = "weerstand";
"RPM" = "rpm";
"ReEnterPassWordTip" = "Het door u ingevoerde wachtwoord komt niet overeen. Voer uw wachtwoord opnieuw in";
"Required" = "Verplicht";
"Revolutions" = "Cadans";
"Recently" = "Kortgeleden";
"ResistanceDifference" = "Weerstandsverschil";
"Rower" = "Roei Ergometer";


/* 
 MARK:S
 */ 

"Save" = "Opslaan";
"Search" = "Zoeken";
"SearchingText" = "Zoeken. . .";
"SeeAll" = "Alles weergeven";
"Segmentation" = "Segmenten";
"SelectEquipment" = "Apparatuur selecteren";
"SelectSegmentation" = "Segmenten selecteren";
"Share" = "Delen";
"Speed" = "Snelheid";
"SpeedUnit" = "Snelheid\n(km/uur)";
"Sprint" = "SPRINT";
"Sprint8" = "Sprint 8";
"SprintSummary" = "Maximale resultaten. Minimale tijd.";
"Start" = "Start";
"Summary" = "Samenvatting";
"SignIn" = "Aanmelden";
"SignUp" = "Inschrijven";
"SignInToatZone" = "Aanmelden";
"Step" = "Stap";
"Send" = "Stuur";
"SendNewCode" = "Stuur een nieuwe code";
"SentPasswordLink" = "We hebben de authenticatiecode zojuist gestuurd naar";
"SearchWorks" = "Zoek werk";
"Sprints" = "SPRINT";
"SearchWorkouts" = "Zoek trainingen";
"StartingSetting" = "Instelling starten";
"StartingSpeed" = "Startsnelheid";
"StartingIncline" = "Helling starten";
"ShowMyRegionOnly" = "Laat alleen mijn land zien";
"SafetyKeyUnplug" = "Veiligheidssleutel loskoppelen";
"SafetyKeyUnplugSummary" = "Herstel de veiligheidssleutel en start de training opnieuw.";
"SoftwareVersion" = "Softwareversies";
"SweatScore" = "Sweat Score";
"StartWorkoutFailed" = "Training beginnen mislukt";
"StartWorkoutFailedSummary" = "Zorg ervoor dat uw machine is gestopt. Druk 3 seconden op de stop-knop op het apparaat om te stoppen.";
"SpeedDefference" = "Snelheidsverschil";
"SelectFromAlbum" = "Selecteren uit album";
"SpeedDifference" = "Snelheidsverschil";
"SelectPhotoSource" = "Fotobron selecteren";
"SuccessfullyConnected" = "succesvol verbonden";
"StartingSpeedSummary" = "Startsnelheid%@";
"SprintInterval" = "Sprint Interval";
"Sprint8TotalWorkoutTimeSummary" = "Totale trainingstijd: 20 minuten";
"StartingResistance" = "Initiële weerstand";
"StepClimber" = "Stepper";
"StairClimber" = "Klimmolen";
"ShareSummary" = "Ik heb zojuist een trainingssessie met @Zone!";


/* 
 MARK:T
 */ 

"Target" = "DOELSTELLING";
"TargetHeartRate" = "Doelhartslag";
"TargetHeartRateSummary" = "De training wordt automatisch aangepast aan de hartslag.";
"Time" = "Tijd";
"TimeSummary" = "Hoe lang houdt u het vol?";
"TotalSweatScore" = "Total Sweat Score";
"TotalDistance" = "TOTALE AFSTAND";
"TotalTime" = "TOTALE TIJD";
"TotalCalories" = "TOTAAL AANTAL CALORIEËN";
"TotalWorkouts" = "Totaal trainingen";
"Training" = "Training";
"Treadmill" = "Lopband";
"TargetDistance" = "AFSTANDSDOEL";
"TargetCalories" = "CALORIEDOEL";
"TargetTime" = "Tijdsdoel";
"TimeGoal" = "Tijdsdoel";
"TryAgain" = "Probeer het opnieuw";
"TermsofUse" = "Gebruiksvoorwaarden";
"TreadmilWithIncine" = "Lopband(kan stijgen)";


/* 
 MARK:U
 */ 

"User" = "Gebruiker";
"Used" = "Besteed";
"UnstableNetwork" = "Instabiel netwerk.";


/* 
 MARK:V
 */ 

"ViewAll" = "Alles weergeven";
"VirtualActive" = "Virtual Active";
"VirtualActiveCourses" = "Virtual Active runs";
"VirtualActiveSummary" = "Virtual Active maakt van elke training een frisse en unieke ervaring door onze speciale interactieve programma's toe te voegen.";
"VeifyEmail" = "Verifieer uw e-mailadres";
"VeifyPassword" = "Verifieer authenticatiecode";
"VideoGuide" = "Videogids";
"VAMinutes" = "%d minuten resterend";
"VAMinutesSummary" = "Leden krijgen 300 minuten videomateriaal per maand gratis. 
";
"VAMinutesConfirmSummary" = " Weet u zeker dat u deze video afspeelt?";
"VAMinutesNotEnoughSummary" = "Leden krijgen 300 minuten videomateriaal per maand gratis. 
Probeer het volgende maand opnieuw of selecteer een kortere video.";
"VAMinutesConfirmButton" = "Start";
"VerificationErrorSummary" = "Factuurverificatie mislukt";


/* 
 MARK:W
 */ 

"Warmup" = "Warm-up";
"Weekly" = "Wekelijks";
"Weight" = "Gewicht";
"Workout" = "Workout";
"Workouts" = "Trainingen";
"WorkoutHistory" = "Geschiedenis training";
"WorkoutSetup" = "Instellen training";
"WorkoutTime" = "Trainingsduur";
"WorkoutSummary" = "Samenvatting training";
"WhatNewPassword" = "Wat moet uw nieuwe wachtwoord worden?";
"WelcomeBack" = "Welkom terug";
"Welcome" = "Welkom";
"WorkoutSettingSummary" = "Stel de startsnelheid en helling in voor deze training";
"WorkoutAborted" = "Workout stopgezet: Geen hartslag gevonden";
"Watts" = "Watts";


/* 
 MARK:X
 */ 



/* 
 MARK:Y
 */ 

"Yearly" = "Jaarlijks";
"YouDesc" = "U (%@)";


/* 
 MARK:Z
 */ 

"ZpointsTitle" = "Z Minute";
"ZpointsWorkoutEarnSummary" = "";
"ZpointsWorkoutEarnHeadline" = "As immersive as it is inspiring";
"ZpointsWorkoutEarnSubline" = "";
"ZpointsPurchaseTitle" = "Z Minute Aankoopplan";
"ZpointsUsageAgreement" = "FAQ";
"ZpointsStoredValueAgreement" = "Gebruiksvoorwaarden";
"ZpointsHistoryTitle" = "Z Minute Geschiedenis";
"ZpointsCostTitle" = "Deze video kost je ";
"ZpointsCostSummary" = "Je hebt meer Z Minute nodig om deze video af te spelen.";


/* 
 MARK:Continent
 */ 



/* 
 MARK:Region
 */ 



/*
  MARK: Language
 */


"en" = "English";
"zh" = "中文";
"zh-Hant" = "繁體中文";
"zh-Hans" = "简体中文";
"de" = "Deutsche";
"nl" = "Nederlands";
"fr" = "Français";
"es" = "Español";
"ja" = "日本語";
"pt" = "Português";
"th" = "ไทย";
"ar" = "عربى";
