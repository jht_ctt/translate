import xlrd


def translateFileforiOS(path, lang, data):
    file = open(path, 'w')
    file.write('/*\n  Localizable.strings\n  FitDisplay\n  Created by Steve Liao on 2020/7/20.\n  Copyright © 2020 Johnsonfitness. All rights reserved.\n*/\n\n\n\n')

    for obj in data:
        if obj["Item Name"] != '':
            if obj["Item Name"].startswith('#'):
                file.write('\n\n/* \n MARK:' + obj["Item Name"].replace('#','')  + '\n */ \n\n' )
            else:
                file.write('"' + obj["Item Name"] + '" = "' + (obj[lang].replace('%s', '%@') if obj[lang] != '' else obj["English"]) + '";\n' )

    file.write('\n\n/*\n  MARK: Error code\n */\n\n\n')
    for obj in data:
        if obj["Backend_Errorcode"] != '':
            result = str(obj["Backend_Errorcode"]).split(",")
            for item in result:
              file.write('"' + 'E' + str(item) + '" = "' + (obj[lang].replace('%s', '%@') if obj[lang] != '' else obj["English"]) + '";\n' )
                    

    file.write('\n\n/*\n  MARK: Language\n */\n\n\n')
    file.write('"en" = "English";\n')
    file.write('"zh" = "中文";\n')
    file.write('"zh-Hant" = "繁體中文";\n')
    file.write('"zh-Hans" = "简体中文";\n')
    file.write('"de" = "Deutsch";\n')
    file.write('"nl" = "Nederlands";\n')
    file.write('"fr" = "Français";\n')
    file.write('"es" = "Español";\n')
    file.write('"ja" = "日本語";\n')
    file.write('"pt" = "Português";\n')
    file.write('"th" = "ภาษาไทย";\n')
    file.write('"ar" = "العربية";\n')
    file.write('"it" = "Italiano";\n')
    file.write('"ko" = "한국인";\n')

    file.close()
    
def translateFileforiOSInfoPlist(path, lang, data):
    file = open(path, 'w')
    file.write('/*\n  InfoPlist.strings\n  FitDisplay\n  Created by Johnson Fitness on 2022/11/15.\n  Copyright © 2022 Johnsonfitness. All rights reserved.\n*/\n\n\n\n')

    for obj in data:
        if obj["Item Name"] != '':
            if obj["Item Name"].startswith('#'):
                file.write('\n\n/* \n MARK:' + obj["Item Name"].replace('#','')  + '\n */ \n\n' )
            else:
                file.write('"' + obj["Item Name"] + '" = "' + (obj[lang] if obj[lang] != '' else obj["English"]) + '";\n' )

    file.close()

def translateFileforAndroid(path, lang, data):
    file = open(path, 'w')
    file.write('<resources>\n')

    for obj in data:
        if obj["Item Name"] != '':
            if obj["Item Name"].startswith('#'):
                file.write('\n\n <!--  ' + obj["Item Name"] + ' --> \n\n' )
            else:
                file.write('<string name="' + obj["Item Name"].replace('.','_') +'">' + (obj[lang].replace('%@', '%1').replace("'","\\'") if obj[lang] != '' else obj["English"].replace('%@','%1').replace("'","\\'")) + '</string>\n')
    
    for obj in data:
        if obj["Backend_Errorcode"] != '':
            result = str(obj["Backend_Errorcode"]).split(",")
            for item in result:
              file.write('<string name="' + 'E' + str(item).replace('.','_') +'">' + (obj[lang].replace('%@', '%1').replace("'","\\'") if obj[lang] != '' else obj["English"].replace('%@','%1').replace("'","\\'")) + '</string>\n')

    file.write('</resources>\n')



if __name__=="__main__":

    workbook = xlrd.open_workbook('translate.xlsx')
    worksheet = workbook.sheet_by_name("UI")
    first_row = [] # The row where we stock the name of the column
    for col in range(worksheet.ncols):
        first_row.append( worksheet.cell_value(0,col) )
    # tronsform the workbook to a list of dictionnary
    data =[]
    for row in range(1, worksheet.nrows):
        elm = {}
        for col in range(worksheet.ncols):
            elm[first_row[col]]=worksheet.cell_value(row,col)
        data.append(elm)

    translateFileforiOS('outfile/iOS/en.lproj/Localizable.strings','English', data)
    translateFileforiOS('outfile/iOS/zh-Hant.lproj/Localizable.strings','繁體中文 Traditional Chinese', data)
    translateFileforiOS('outfile/iOS/zh-Hans.lproj/Localizable.strings','簡體中文 Simple Chinese', data)
    translateFileforiOS('outfile/iOS/de.lproj/Localizable.strings','German', data)
    translateFileforiOS('outfile/iOS/nl.lproj/Localizable.strings','Dutch', data)
    translateFileforiOS('outfile/iOS/fr.lproj/Localizable.strings','French', data)
    translateFileforiOS('outfile/iOS/es.lproj/Localizable.strings','Spanish', data)
    translateFileforiOS('outfile/iOS/ja.lproj/Localizable.strings','Japanese', data)
    translateFileforiOS('outfile/iOS/it-IT.lproj/Localizable.strings','Italian', data)
    translateFileforiOS('outfile/iOS/ko.lproj/Localizable.strings','Korean', data)
    translateFileforiOS('outfile/iOS/ar.lproj/Localizable.strings','Arabic', data)
    translateFileforiOS('outfile/iOS/pt-PT.lproj/Localizable.strings','Portuguese', data)
    translateFileforiOS('outfile/iOS/th.lproj/Localizable.strings','Thai', data)

    translateFileforAndroid('outfile/android/values-en/strings.xml','English',data)
    translateFileforAndroid('outfile/android/values-zh-rTW/strings.xml','繁體中文 Traditional Chinese',data)
    translateFileforAndroid('outfile/android/values-zh-rCN/strings.xml','簡體中文 Simple Chinese',data)
    translateFileforAndroid('outfile/android/values-de/strings.xml','German',data)
    translateFileforAndroid('outfile/android/values-nl/strings.xml','Dutch',data)
    translateFileforAndroid('outfile/android/values-fr/strings.xml','French',data)
    translateFileforAndroid('outfile/android/values-es/strings.xml','Spanish',data)
    translateFileforAndroid('outfile/android/values-ja/strings.xml','Japanese',data)
    translateFileforAndroid('outfile/android/values-it-rIT/strings.xml','Italian',data)
    translateFileforAndroid('outfile/android/values-ko/strings.xml','Korean',data)
    translateFileforAndroid('outfile/android/values-ar/strings.xml','Arabic',data)
    translateFileforAndroid('outfile/android/values-pt-rPT/strings.xml','Portuguese',data)
    translateFileforAndroid('outfile/android/values-th/strings.xml','Thai',data)

    worksheet2 = workbook.sheet_by_name("iOS_InfoPlist")
    first_row2 = [] # The row where we stock the name of the column
    for col in range(worksheet2.ncols):
        first_row2.append( worksheet2.cell_value(0,col) )
    # tronsform the workbook to a list of dictionnary
    data2 =[]
    for row in range(1, worksheet2.nrows):
        elm = {}
        for col in range(worksheet2.ncols):
            elm[first_row2[col]]=worksheet2.cell_value(row,col)
        data2.append(elm)

    translateFileforiOSInfoPlist('outfile/iOS/en.lproj/InfoPlist.strings','English', data2)
    translateFileforiOSInfoPlist('outfile/iOS/zh-Hant.lproj/InfoPlist.strings','繁體中文 Traditional Chinese', data2)
    translateFileforiOSInfoPlist('outfile/iOS/zh-Hans.lproj/InfoPlist.strings','簡體中文 Simple Chinese', data2)
    translateFileforiOSInfoPlist('outfile/iOS/de.lproj/InfoPlist.strings','German', data2)
    translateFileforiOSInfoPlist('outfile/iOS/nl.lproj/InfoPlist.strings','Dutch', data2)
    translateFileforiOSInfoPlist('outfile/iOS/fr.lproj/InfoPlist.strings','French', data2)
    translateFileforiOSInfoPlist('outfile/iOS/es.lproj/InfoPlist.strings','Spanish', data2)
    translateFileforiOSInfoPlist('outfile/iOS/ja.lproj/InfoPlist.strings','Japanese', data2)
    translateFileforiOSInfoPlist('outfile/iOS/it-IT.lproj/InfoPlist.strings','Italian', data2)
    translateFileforiOSInfoPlist('outfile/iOS/ko.lproj/InfoPlist.strings','Korean', data2)
    translateFileforiOSInfoPlist('outfile/iOS/ar.lproj/InfoPlist.strings','Arabic', data2)
    translateFileforiOSInfoPlist('outfile/iOS/pt-PT.lproj/InfoPlist.strings','Portuguese', data2)
    translateFileforiOSInfoPlist('outfile/iOS/th.lproj/InfoPlist.strings','Thai', data2)










